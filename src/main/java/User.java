
import java.util.Collections;
import java.util.List;

public class User implements java.io.Serializable {
    public String UserName;
    public String FirstName;
    public String LastName;



    List<Task> taskList = Collections.emptyList();

    public User(String FirstName, String LastName, String UserName){
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.UserName = UserName;
    }

    private void addTask(Task task){
        taskList.add(task);
    }

}
