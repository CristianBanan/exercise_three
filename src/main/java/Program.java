import java.sql.*;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws SQLException {
        // JDBC driver name and database URL
         DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
         final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
         final String DB_URL = "jdbc:mysql://localhost:3306/taskmanager";

        // Database credentials
         final String USER = "root";
         final String PASS = "";

        Connection conn = null;
        Statement stmt = null;
        Scanner scn = new Scanner(System.in);
        String course_code = null, course_desc = null, course_chair = null;

        try {
            // STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            // STEP 3: Open a connection
            System.out.print("\nConnecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println(" SUCCESS!\n");


            if (args[0].equals("-createUser")) {
                // the mysql insert statement
                String query = " insert into users (UserName, FirstName, LastName)"
                        + " values (?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setString (1, args[3]);
                preparedStmt.setString (2, args[1]);
                preparedStmt.setString  (3, args[2]);


                // execute the preparedstatement
                preparedStmt.execute();

                conn.close();

            }
            else if (args[0].equals("-showAllUsers")) {
                stmt = conn.createStatement();
                String query = "SELECT * FROM User";
                ResultSet rs=stmt.executeQuery(query);
                System.out.println("UserName"+"\t"+"FirstName"+"\t"+"LastName");
                //Extact result from ResultSet rs
                while(rs.next()){
                    System.out.println(""+rs.getString("UserName")+"\t"+rs.getString("FirstName")+rs.getString("LastName"));
                }

            }

            else if (args[0].equals("-addTask")) {
                // the mysql insert statement
                String query = " insert into task (UserName, title, description)"
                        + " values (?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setString (1, args[1]);
                preparedStmt.setString (2, args[2]);
                preparedStmt.setString  (3, args[3]);


                // execute the preparedstatement
                preparedStmt.execute();

                conn.close();
            }

            else if (args[0].equals("-showTasks")) {
                String query = "SELECT * FROM Task WHERE UserName = ?";
                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setString (1, args[1]);

                // execute the preparedstatement
                ResultSet rs = preparedStmt.executeQuery();

                System.out.println("Title"+"\t"+"Description");
                //Extact result from ResultSet rs
                while(rs.next()){
                    System.out.println(""+rs.getString("Title")+"\t"+rs.getString("Description"));
                }

            }

            else if (args[0].equals("-taskAssignedToMultipleUsers")) {
            }



        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }






    }
}
